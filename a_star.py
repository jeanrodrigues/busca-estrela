import dists

def a_star(start, goal="Bucharest"):
    """
    Retorna uma lista com o caminho de start até 
    goal segundo o algoritmo A*
    """
    
    custo = 0
    heuristica = dists.straight_line_dists_from_bucharest[start]
    caminho = []
    no_inicial = (start,custo,heuristica,caminho)
    
    return busca_proximo(no_inicial,goal,[])
   

def busca_proximo(no_atual,goal,nos):
    
    atual, distancia, heuristica, caminho = no_atual
    
    if(atual==goal):
        print(distancia)
        return caminho

    vizinhos = dists.dists[atual]

    for vizinho in vizinhos:
        nome_vizinho, distancia_vizinho = vizinho
        distancia_estimada = dists.straight_line_dists_from_bucharest[nome_vizinho]
        distancia_percorrida = distancia + distancia_vizinho
        heuristica_vizinho = distancia_percorrida + distancia_estimada
        nos += [(nome_vizinho,distancia_percorrida,heuristica_vizinho,caminho+[nome_vizinho])]

    proximo_no = nos[0]

    for no in nos:
        if( no[2] < proximo_no[2] ):
            proximo_no = no

    nos.remove(proximo_no)
    
    return busca_proximo(proximo_no,goal,nos)

